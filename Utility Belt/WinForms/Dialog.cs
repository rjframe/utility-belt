﻿/*
 * 
 * Created by: Ryan Frame
 * Date: 1/27/2014
 * Last modified by: Ryan Frame
 * Date: 1/27/2014
 *
 */
using System;
using System.Windows.Forms;

namespace Simplify.Utility_Belt.WinForms
{
  /// <summary>Interface/instance to mock the MessageBox when needed</summary>
  public interface IDialog
  {
    DialogResult ShowMessageBox(String message);
    DialogResult ShowMessageBox(String message, String caption);
    DialogResult ShowMessageBox(String message, String caption, MessageBoxButtons buttons);
  }
  
  public class Dialog : IDialog
  {
    public Dialog()
    {
    }
    
    public DialogResult ShowMessageBox(String message)
    {
      return MessageBox.Show(message);
    }
    
    public DialogResult ShowMessageBox(String message, String caption)
    {
      return MessageBox.Show(message, caption);
    }
    
    public DialogResult ShowMessageBox(String message, String caption, MessageBoxButtons buttons)
    {
      return MessageBox.Show(message, caption, buttons);
    }
  }
}
