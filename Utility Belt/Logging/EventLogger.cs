﻿/*
 * 
 * Created by: Ryan Frame
 * Date: 1/27/2014
 * Last modified by: Ryan Frame
 * Date: 1/27/2014
 *
 */
using System;
using System.Diagnostics;

namespace Simplify.Utility_Belt.Logging
{
  public interface IEventLog
  {
    void WriteEntry(String message, EventLogEntryType type);
    void WriteEntry(String message, EventLogEntryType type, int eventID);
    void Info(String message);
    void Info(String message, int eventID);
    void Warn(String message);
    void Warn(String message, int eventID);
    void Error(String message);
    void Error(String message, int eventID);
    void ErrorWithTrace(String message, Exception exception);
    void ErrorWithTrace(String message, Exception exception, int eventID);
    void StackTrace(Exception exception);
    void StackTrace(Exception exception, int eventID);
  }

  public class EventLogHandler : IEventLog
  {
    private EventLog _log;

    public enum Logs
    {
      Application,
      System,
      Security,
    }

    #region Constructors

    public EventLogHandler(EventLog eventLog)
    {
      _log = eventLog;
    }

    /// <summary>Creates a new EventLogHandler</summary>
    /// <param name="source"></param>
    /// <param name="log"></param>
    /// <exception cref="ArgumentException"></exception>
    /// <exception cref="InvalidOperationException"></exception>
    public EventLogHandler(String source, String log)
    {
      if (!EventLog.SourceExists(source))
      {
        EventLog.CreateEventSource(source, log);
      }
      _log.Source = source;
      _log.Log = log;
    }

    /// <summary>Creates a new EventLogHandler</summary>
    /// <param name="source"></param>
    /// <param name="log"></param>
    /// <exception cref="ArgumentException"></exception>
    /// <exception cref="InvalidOperationException"></exception>
    public EventLogHandler(String source, Logs log)
      : this(source, Enum.GetName(typeof(Logs), log))
    { }

    #endregion Constructors

    #region Interface methods

    public void WriteEntry(String message, EventLogEntryType type)
    {
      _log.WriteEntry(message, type);
    }

    public void WriteEntry(String message, EventLogEntryType type, int eventID)
    {
      _log.WriteEntry(message, type, eventID);
    }

    public void Info(String message)
    {
      _log.WriteEntry(message, EventLogEntryType.Information);
    }

    public void Info(String message, int eventID)
    {
      _log.WriteEntry(message, EventLogEntryType.Information, eventID);
    }

    public void Warn(String message)
    {
      _log.WriteEntry(message, EventLogEntryType.Warning);
    }

    public void Warn(String message, int eventID)
    {
      _log.WriteEntry(message, EventLogEntryType.Warning, eventID);
    }

    public void Error(String message)
    {
      _log.WriteEntry(message, EventLogEntryType.Error);
    }

    public void Error(String message, int eventID)
    {
      _log.WriteEntry(message, EventLogEntryType.Error, eventID);
    }

    public void ErrorWithTrace(String message, Exception exception)
    {
      _log.WriteEntry(message, EventLogEntryType.Error);
      _log.WriteEntry(exception.StackTrace, EventLogEntryType.Error);
    }

    public void ErrorWithTrace(String message, Exception exception, int eventID)
    {
      _log.WriteEntry(message, EventLogEntryType.Error, eventID);
      _log.WriteEntry(exception.StackTrace, EventLogEntryType.Error, eventID);
    }

    public void StackTrace(Exception exception)
    {
      _log.WriteEntry(exception.StackTrace, EventLogEntryType.Error);
    }

    public void StackTrace(Exception exception, int eventID)
    {
      _log.WriteEntry(exception.StackTrace, EventLogEntryType.Error, eventID);
    }

    #endregion Interface methods
  }

  /// <summary>
  /// Throws the log messages away
  /// </summary>
  public class DummyLogger : IEventLog
  {
    public void WriteEntry(string message, EventLogEntryType type) { }

    public void WriteEntry(string message, EventLogEntryType type, int eventID) { }

    public void Info(string message) { }

    public void Info(string message, int eventID) { }

    public void Warn(string message) { }

    public void Warn(string message, int eventID) { }

    public void Error(string message) { }

    public void Error(string message, int eventID) { }

    public void ErrorWithTrace(string message, Exception exception) { }

    public void ErrorWithTrace(string message, Exception exception, int eventID) { }

    public void StackTrace(Exception exception) { }

    public void StackTrace(Exception exception, int eventID) { }
  }
}
