﻿/*
 * 
 * Created by: Ryan Frame
 * Date: 1/27/2014
 * Last modified by: Ryan Frame
 * Date: 1/27/2014
 *
 */
using System;

using TechTalk.SpecFlow;

namespace Simplify.Utility_Belt.Tests.SpecFlow
{
  /// <summary>
  /// Shorthand for accessing the Scenario.Current dictionary
  /// </summary>
  /// <remarks>From McEntyre321 at StackOverflow.
  /// <srcurl>http://stackoverflow.com/a/13971643/1991068</srcurl><c/remarks>
  public static class Current<T> where T : class
  {
    internal static T Value
    {
      get {
        return ScenarioContext.Current.ContainsKey(typeof(T).FullName)
          ? ScenarioContext.Current[typeof(T).FullName] as T : null;
      }
      set { ScenarioContext.Current[typeof(T).FullName] = value; }
    }
  }
}
