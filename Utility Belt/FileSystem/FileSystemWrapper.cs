﻿/*
 * 
 * Created by: Ryan Frame
 * Date: 1/27/2014
 * Last modified by: Ryan Frame
 * Date: 1/27/2014
 *
 */
using System;
using System.IO;

namespace Simplify.Utility_Belt.FileSystem
{
  /// <summary>For mocking the filesystem</summary>
  public interface IFile
  {
    bool FileExists(String path);
    void DeleteFile(String path);
  }

  public class FSWrapper : IFile
  {
    public Boolean FileExists(String path)
    {
      return File.Exists(path);
    }

    public void DeleteFile(String path)
    {
      File.Delete(path);
    }
  }
}
