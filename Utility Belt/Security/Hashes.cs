﻿/*
 * 
 * Created by: Ryan Frame
 * Date: 1/27/2014
 * Last modified by: Ryan Frame
 * Date: 1/27/2014
 *
 */
using System;
using System.Text;
using System.IO;
using System.Security;
using System.Security.Cryptography;

using Simplify.Utility_Belt.Security;

namespace Simplify.Utility_Belt.Security.Cryptography
{
  public static class Hash
  {
    
    #region SHA256
    
    /// <summary>Returns the SHA256 hash of the given password and salt combination</summary>
    /// <param name="pass"></param>
    /// <param name="salt"></param>
    /// <remarks>Note that the password is converted to a String then a byte array, potentially
    ///     revealing its value. To hash a salt-less password, use an empty string for the salt.
    /// </remarks>
    /// <returns></returns>
    public static byte[] HashPassword(SecureString pass, byte[] salt)
    {
      var text = Encoding.Default.GetBytes(pass.ToNormalString());
      byte[] password = new byte[text.Length + salt.Length];
      
      for (int i = 0; i < text.Length; i++)
      {
        password[i] = text[i];
      }
      for (int i = 0; i < salt.Length; i++)
      {
        password[i + text.Length] = salt[i];
      }
      
      return new SHA256Managed().ComputeHash(password);
    }
    
    public static byte[] HashPassword(SecureString pass, String salt)
    {
      return HashPassword(pass, Encoding.Default.GetBytes(salt));
    }
    
    #endregion SHA256
    
    #region MD5
    
    /// <summary>Calculates the MD5 hash of the given data</summary>
    /// <param name="data">The <see cref="byte[]"/> data</param>
    /// <param name="base64">If true, returns a base64-encoded representation of the hash</param>
    /// <returns>The hash of the given data</returns>
    public static String GetMD5(byte[] data, Boolean base64 = false)
    {
      byte[] hash;

      using (MD5 md5 = MD5.Create())
      {
        hash = md5.ComputeHash(data);
      }

      // Returns bas64-encoded hash
      if (base64) return Convert.ToBase64String(hash);

      // Convert the hash to a string and return
      StringBuilder stringHash = new StringBuilder();

      for (int i = 0; i < hash.Length; i++)
      {
        stringHash.Append(hash[i].ToString("x2"));
      }

      return stringHash.ToString();
    }

    /// <summary>Calculates the MD5 hash of the given data</summary>
    /// <param name="data">The <see cref="String"/> data</param>
    /// <param name="base64">If true, returns a base64-encoded representation of the hash</param>
    /// <returns>The hash of the given <see cref="String"/></returns>
    public static String GetMD5(String data, Boolean base64 = false)
    {
      return GetMD5(Encoding.UTF8.GetBytes(data), base64);
    }

    public static String GetMD5(Stream data, Boolean base64 = false)
    {
      byte[] hash;

      using (MD5 md5 = MD5.Create())
      {
        hash = md5.ComputeHash(data);
      }

      // Returns bas64-encoded hash
      if (base64) return Convert.ToBase64String(hash);

      // Convert the hash to a string and return
      StringBuilder stringHash = new StringBuilder();

      for (int i = 0; i < hash.Length; i++)
      {
        stringHash.Append(hash[i].ToString("x2"));
      }

      return stringHash.ToString();
    }

    /// <summary>Calculates the MD5 hash of the given file</summary>
    /// <param name="path">The path to the file</param>
    /// <param name="base64">If true, returns a base64-encoded representation of the hash</param>
    /// <param name="filesystem">The filesystem wrapper (used for tests)</param>
    /// <returns>The hash of the given data</returns>
    public static String GetMD5OfFile(String path, Boolean base64 = false)
    {
      try
      {
        using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
        {
          return GetMD5(fs);
        }
      }
      catch (FileNotFoundException)
      {
        throw new NotImplementedException("Implement exception handler in GetMD5OfFile");
      }
    }
    
    #endregion MD5
  }
}