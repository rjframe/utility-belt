﻿/*
 * 
 * Created by: Ryan Frame
 * Date: 1/27/2014
 * Last modified by: Ryan Frame
 * Date: 1/27/2014
 *
 */
using System;
using System.Security;
using System.Runtime.InteropServices;

namespace Simplify.Utility_Belt.Security
{
  /// <summary>
  /// SecureString extension methods for conversions
  /// </summary>
  public static class SecureStringExtensionMethods
  {
    /// <summary>Converts a <see cref="String"/> to a <see cref="SecureString" /></summary>
    public static String ToNormalString(this SecureString fromSecureString)
    {
      if (fromSecureString == null)
        throw new NullReferenceException("The SecureString cannot be null");

      IntPtr unmanagedString = IntPtr.Zero;
      try
      {
        unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(fromSecureString);
        return Marshal.PtrToStringUni(unmanagedString);
      }
      finally
      {
        Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
      }
    }

    /// <summary>Converts a <see cref="String"/> to a <see cref="SecureString"/></summary>
    /// <param name="theString"></param>
    /// <returns></returns>
    public static SecureString ToSecureString(this String theString, Boolean makeReadOnly = true)
    {
      unsafe
      {
        fixed (char* charString = theString)
        {
          SecureString secureString = new SecureString(charString, theString.Length);
          if (makeReadOnly)
          {
            secureString.MakeReadOnly();
          }
          return secureString;
        }
      }
    }
  }
}